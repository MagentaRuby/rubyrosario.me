---
title: Realm of the Winds
---

Greetings internet, Ruby here. Thank you for taking an interest in my server. There are a lot of Minecraft servers these days, and most of them are… let’s just leave it at inadequate. While I can’t guarantee that this server will be “the best”, I do try my best to create a high quality, fun, and unique experience. Hear me out, and I’ll explain what will make this server different from the rest. As for whether it’s better, that’s up to you.  

# Features

- Custom plugins at the core containing only the functionality that we actually use, and none of the extra bloat.
- Custom Skylands World Generation
- Everyone is given a free elytra (wings) that repair themselves.
- If you fall in the void, you respawn instantly at the last place you were standing (with a health penalty).
- A bed is required to set home.
- PvP is always on in the survival realm.
- Custom, nearly-automated anti-grief plugin.
  - Build it, claim it, add your friends, and done!
- Custom teleportation plugin.
  - Warps are created with an altar that you can buy with gold/emeralds.
  - Anyone can teleport to any altar that they have found or bought.
  - Your location in each of the realms is automatically saved.
  - Fancy teleport effects can be bought with Wind Tokens.

# Un-features

_Things that most servers do, but we refuse to do._  

- **Pay-to-Win**: It seems like every server out there now has at least some pay-to-win things in it, but we’ll always keep the donator perks fair.
- **Cheaty Kits**: That means that players will not be given mid to op tier items for free. Also, items will not be given through commands unless they are purely informational and/or cosmetic. Players will not start the game with any items that aren’t absolutely necessary.
- **Cheaty Teleports**: That means all teleports (unless you’re in the creative world) will take at least 5 seconds, and cost xp. Also, no /warps and no multiple homes.
- **Locks**: No container locks of any kind. Anyone can steal if they can get to the container.
- **Manual Claiming**: Most anti-grief plugins (like GriefPrevention) make you select the exact area that is protected using a tool like a golden shovel at best, or confusing commands at worst. They’re also either completely unlimited or limited by some completely unrelated metric like play time or virtual currency. Wouldn’t it be simpler if it would just keep track of where you’re building? Now all you have to do is tell the plugin to claim the building and invite your friends (if you want to).